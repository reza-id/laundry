<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\ServiceItem;

/**
 * ServiceItemSearch represents the model behind the search form about `common\models\ServiceItem`.
 */
class ServiceItemSearch extends ServiceItem
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'company_id'], 'integer'],
            [['item_name', 'service_ids', 'company.company_name'], 'safe'],
        ];
    }

    public function attributes()
    {
        // add related fields to searchable atributes
        return array_merge(parent::attributes(), ['company.company_name']);
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ServiceItem::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        /**
         * Setup your sorting attributes
         * Note: This is setup before the $this->load($params) 
         * statement below
         */
        $dataProvider->setSort([
            'attributes' => [
                'id',
                'company.company_name',
                'item_name',
                'service_ids'
            ]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->joinWith(['company']);
        $query->andFilterWhere(
            ['LIKE', 'company.company_name', $this->getAttribute('company.company_name')]
        );

        $query->andFilterWhere([
            'id' => $this->id,
        ]);

        $query->andFilterWhere(['like', 'item_name', $this->item_name])
            ->andFilterWhere(['like', 'service_ids', $this->service_ids]);

        return $dataProvider;
    }
}
