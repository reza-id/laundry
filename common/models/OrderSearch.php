<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Order;

/**
 * OrderSearch represents the model behind the search form about `common\models\Order`.
 */
class OrderSearch extends Order
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'company_id', 'user_id', 'order_paid', 'order_status', 'service_id'], 'integer'],
            [['order_date', 'order_start', 'order_finish'], 'safe'],
            [['order_amount', 'order_price'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Order::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'company_id' => $this->company_id,
            'user_id' => $this->user_id,
            'order_date' => $this->order_date,
            'order_start' => $this->order_start,
            'order_finish' => $this->order_finish,
            'order_amount' => $this->order_amount,
            'order_price' => $this->order_price,
            'order_paid' => $this->order_paid,
            'order_status' => $this->order_status,
            'service_id' => $this->service_id,
        ]);

        return $dataProvider;
    }
}
