<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "service_items".
 *
 * @property integer $id
 * @property integer $company_id
 * @property string $item_name
 * @property string $service_ids
 *
 * @property OrderItems[] $orderItems
 * @property Company $company
 */
class ServiceItem extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'service_items';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['company_id', 'item_name', 'service_ids'], 'required'],
            [['company_id'], 'integer'],
            [['item_name'], 'string', 'max' => 32],
            [['service_ids'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'company_id' => Yii::t('app', 'Company ID'),
            'item_name' => Yii::t('app', 'Item Name'),
            'service_ids' => Yii::t('app', 'Service Ids'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrderItems()
    {
        return $this->hasMany(OrderItems::className(), ['item_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Company::className(), ['id' => 'company_id']);
    }
}
