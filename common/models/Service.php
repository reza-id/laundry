<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "services".
 *
 * @property integer $id
 * @property integer $company_id
 * @property string $service_name
 * @property string $service_unit
 * @property string $price_per_unit
 *
 * @property Company $company
 */
class Service extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'services';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['company_id', 'service_name', 'service_unit', 'price_per_unit'], 'required'],
            [['company_id'], 'integer'],
            [['price_per_unit'], 'number'],
            [['service_name'], 'string', 'max' => 32],
            [['service_unit'], 'string', 'max' => 10]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'company_id' => Yii::t('app', 'Company ID'),
            'service_name' => Yii::t('app', 'Service Name'),
            'service_unit' => Yii::t('app', 'Service Unit'),
            'price_per_unit' => Yii::t('app', 'Price Per Unit'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Company::className(), ['id' => 'company_id']);
    }
}
