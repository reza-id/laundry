<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Service;

/**
 * ServiceSearch represents the model behind the search form about `common\models\Service`.
 */
class ServiceSearch extends Service
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'company_id'], 'integer'],
            [['service_name', 'service_unit', 'company.company_name'], 'safe'],
            [['price_per_unit'], 'number'],
        ];
    }

    public function attributes()
    {
        // add related fields to searchable atributes
        return array_merge(parent::attributes(), ['company.company_name']);
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Service::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        /**
         * Setup your sorting attributes
         * Note: This is setup before the $this->load($params) 
         * statement below
         */
        $dataProvider->setSort([
            'attributes' => [
                'id',
                'company.company_name',
                'service_name',
                'service_unit',
                'price_per_unit'
            ]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->joinWith(['company']);
        $query->andFilterWhere(
            ['LIKE', 'company.company_name', $this->getAttribute('company.company_name')]
        );

        $query->andFilterWhere([
            'id' => $this->id,
            'company_id' => $this->company_id,
            // 'price_per_unit' => $this->price_per_unit,
        ]);
        
        $query->andFilterWhere(
            ['LIKE', 'price_per_unit', $this->getAttribute('price_per_unit')]
        );

        $query->andFilterWhere(['like', 'service_name', $this->service_name])
            ->andFilterWhere(['like', 'service_unit', $this->service_unit]);

        return $dataProvider;
    }
}
