<?php
namespace common\models;

use Yii;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;

/**
 * User model
 *
 * @property integer $id
 * @property integer $company_id
 * @property string $handphone
 * @property string $user_fullname
 * @property string $password_hash
 * @property string $password_reset_token
 * @property string $auth_key
 * @property string $access_token
 * @property string $user_address
 * @property string $user_longitude
 * @property string $user_latitude
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 * @property string $password write-only password
 *
 * @property Orders[] $orders
 * @property Company $company
 */
class User extends ActiveRecord implements IdentityInterface
{
    const STATUS_DELETED = 0;
    const STATUS_ACTIVE = 10;

    public $password;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%user}}';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['status', 'default', 'value' => self::STATUS_ACTIVE],
            ['status', 'in', 'range' => [self::STATUS_ACTIVE, self::STATUS_DELETED]],
            [['company_id', 'handphone', 'user_fullname'], 'required'],
            [['company_id', 'handphone'], 'unique', 'targetAttribute' => ['company_id', 'handphone'], 'message' => 'Nomer HP sudah terdaftar!'],

            ['password', 'required', 'on' => 'create'],
            ['password', 'string', 'min' => 4, 'skipOnEmpty' => true],

            [['user_address', 'user_longitude', 'user_latitude'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
           'id' => Yii::t('app', 'ID'),
           'company_id' => Yii::t('app', 'Company ID'),
           'handphone' => Yii::t('app', 'Handphone'),
           'user_fullname' => Yii::t('app', 'Full Name'),
           'auth_key' => Yii::t('app', 'Auth Key'),
           'password_hash' => Yii::t('app', 'Password Hash'),
           'password_reset_token' => Yii::t('app', 'Password Reset Token'),
           'access_token' => Yii::t('app', 'Access Token'),
           'user_address' => Yii::t('app', 'User Address'),
           'user_longitude' => Yii::t('app', 'User Longitude'),
           'user_latitude' => Yii::t('app', 'User Latitude'),
           'status' => Yii::t('app', 'Status'),
           'created_at' => Yii::t('app', 'Created At'),
           'updated_at' => Yii::t('app', 'Updated At'),

           'password' => Yii::t('app', 'Password'),
       ];
    }

    /**
     * @inheritdoc
     */
    public static function findIdentity($id)
    {
        return static::findOne(['id' => $id, 'status' => self::STATUS_ACTIVE]);
    }

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        return static::findOne(['access_token' => $token]);
    }

    /**
     * Finds user by username
     *
     * @param string $handphone
     * @return static|null
     */
    public static function findByHandphoneAndCompanyId($handphone, $company_id)
    {
        return static::findOne(['handphone' => $handphone, 'company_id' => $company_id, 'status' => self::STATUS_ACTIVE]);
    }

    /**
     * Finds user by password reset token
     *
     * @param string $token password reset token
     * @return static|null
     */
    public static function findByPasswordResetToken($token)
    {
        if (!static::isPasswordResetTokenValid($token)) {
            return null;
        }

        return static::findOne([
            'password_reset_token' => $token,
            'status' => self::STATUS_ACTIVE,
        ]);
    }

    /**
     * Finds out if password reset token is valid
     *
     * @param string $token password reset token
     * @return boolean
     */
    public static function isPasswordResetTokenValid($token)
    {
        if (empty($token)) {
            return false;
        }

        $timestamp = (int) substr($token, strrpos($token, '_') + 1);
        $expire = Yii::$app->params['user.passwordResetTokenExpire'];
        return $timestamp + $expire >= time();
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->getPrimaryKey();
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return boolean if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return Yii::$app->security->validatePassword($password, $this->password_hash);
    }

    /**
     * Generates password hash from password and sets it to the model
     *
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->password_hash = Yii::$app->security->generatePasswordHash($password);
    }

    /**
     * Generates "remember me" authentication key
     */
    public function generateAuthKey()
    {
        $this->auth_key = Yii::$app->security->generateRandomString();
    }

    /**
     * Generates new password reset token
     */
    public function generatePasswordResetToken()
    {
        $this->password_reset_token = Yii::$app->security->generateRandomString() . '_' . time();
    }

    /**
     * Removes password reset token
     */
    public function removePasswordResetToken()
    {
        $this->password_reset_token = null;
    }

   /**
    * @return \yii\db\ActiveQuery
    */
    public function getOrders()
    {
       return $this->hasMany(Order::className(), ['user_id' => 'id']);
    }

   /**
    * @return \yii\db\ActiveQuery
    */
    public function getCompany()
    {
       return $this->hasOne(Company::className(), ['id' => 'company_id']);
    }

    /**
     * Add new user.
     *
     * @return User|null the saved model or null if saving fails
     */
    public function add()
    {
        $this->scenario = 'create';

        if (!$this->validate()) {
            return null;
        }
        
        $this->setPassword($this->password);
        $this->generateAuthKey();
        
        return $this->save() ? $this : null;
    }

    /**
     * Edit existing user.
     *
     * @return User|null the saved model or null if saving fails
     */
    public function edit()
    {
        if (!$this->validate()) {
            return null;
        }

        if (!empty($this->password)) {
            $this->setPassword($this->password);
            $this->generateAuthKey();
        }        
        
        return $this->save() ? $this : null;
    }
}
