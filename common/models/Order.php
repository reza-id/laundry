<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "orders".
 *
 * @property integer $id
 * @property integer $company_id
 * @property integer $user_id
 * @property string $order_date
 * @property string $order_start
 * @property string $order_finish
 * @property string $order_amount
 * @property string $order_price
 * @property integer $order_paid
 * @property integer $order_status
 * @property integer $service_id
 *
 * @property OrderItems[] $orderItems
 * @property Service $service
 * @property Company $company
 * @property User $user
 */
class Order extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'orders';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['company_id', 'user_id', 'order_date'], 'required'],
            [['company_id', 'user_id', 'order_paid', 'order_status', 'service_id'], 'integer'],
            [['order_date', 'order_start', 'order_finish'], 'safe'],
            [['order_amount', 'order_price'], 'number']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'company_id' => Yii::t('app', 'Company ID'),
            'user_id' => Yii::t('app', 'User ID'),
            'order_date' => Yii::t('app', 'Order Date'),
            'order_start' => Yii::t('app', 'Order Start'),
            'order_finish' => Yii::t('app', 'Order Finish'),
            'order_amount' => Yii::t('app', 'Order Amount'),
            'order_price' => Yii::t('app', 'Order Price'),
            'order_paid' => Yii::t('app', 'Order Paid'),
            'order_status' => Yii::t('app', 'Order Status'),
            'service_id' => Yii::t('app', 'Service ID'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrderItems()
    {
        return $this->hasMany(OrderItem::className(), ['order_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getService()
    {
        return $this->hasOne(Service::className(), ['id' => 'service_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Company::className(), ['id' => 'company_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
}
