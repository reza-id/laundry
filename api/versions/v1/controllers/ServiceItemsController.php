<?php

namespace api\versions\v1\controllers;

use Yii;
use yii\rest\ActiveController;

class ServiceItemsController extends ActiveController
{
	public $modelClass = 'common\models\ServiceItem';
}