<?php

namespace api\versions\v1\controllers;

use Yii;
use yii\rest\ActiveController;
use yii\filters\auth\HttpBearerAuth;

class UsersController extends ActiveController
{
	public $modelClass = 'common\models\User';

	public function behaviors()
	{
		$behaviors = parent::behaviors();

		$behaviors['authenticator'] = [
			'except' => [ 'login', 'register' ],
			'class' => HttpBearerAuth::className(),
		];

		return $behaviors;
	}

	public function actions()
    {
        $actions = parent::actions();
        unset($actions['create']);
        unset($actions['update']);
        return $actions;
    }

	public function actionLogin()
	{
		$model = new \common\models\LoginForm;

		if ($model->load(Yii::$app->request->bodyParams, '') && $model->login()) {
			$user = \Yii::$app->user->identity;
			$user->access_token = Yii::$app->security->generateRandomString();
			$user->save();

			return [
				'access-token' => $user->access_token,
				'user_fullname' => $user->user_fullname,
			];
		} else {
			return $model;
		}
	}

	public function actionRegister()
	{
		$model = new \frontend\models\SignupForm;

		if ($model->load(Yii::$app->request->bodyParams, '') && $user = $model->signup()) {

			$user->access_token = Yii::$app->security->generateRandomString();
			$user->save();

			return [
				'access-token' => $user->access_token,
				'user_fullname' => $user->user_fullname,
			];
		} else {
			return $model;
		}
	}

	public function actionCreate()
	{
		$model = $this->modelClass;
		return $model;
	}

	public function actionUpdate()
	{
		return "update belum yaa";
	}
}