<?php

namespace api\versions\v1\controllers;

use Yii;
use yii\rest\ActiveController;

class CompaniesController extends ActiveController
{
	public $modelClass = 'common\models\Company';
}