<?php

use yii\db\Migration;

class m160307_110319_add_user_fullname extends Migration
{
    public function up()
    {
        $this->addColumn('{{%user}}', 'user_fullname', $this->string()->notNull());
    }

    public function down()
    {
        $this->dropColumn('{{%user}}', 'user_fullname');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
