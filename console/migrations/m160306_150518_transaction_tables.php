<?php

use yii\db\Migration;

class m160306_150518_transaction_tables extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%orders}}', [
            'id' => $this->primaryKey(),
            'company_id' => $this->integer()->notNull(),
            'user_id' => $this->integer()->notNull(),
            'order_date' => $this->date()->notNull(),
            'order_start' => $this->date(),
            'order_finish' => $this->date(),
            'order_amount' => $this->decimal(20, 2),
            'order_price' => $this->decimal(20, 2),
            'order_paid' => $this->boolean(),
            'order_status' => $this->smallInteger()->notNull()->defaultValue(10),
        ], $tableOptions);
        $this->createIndex('idx-orders-company_id', '{{%orders}}', 'company_id');
        $this->addForeignKey('fk-orders-company_id', '{{%orders}}', 'company_id', '{{%company}}', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('fk-orders-user_id', '{{%orders}}', 'user_id', '{{%user}}', 'id', 'CASCADE', 'CASCADE');

        $this->createTable('{{%order_items}}', [
            'id' => $this->primaryKey(),
            'order_id' => $this->integer()->notNull(),
            'item_id' => $this->integer()->notNull(),
            'amount' => $this->integer()->notNull(),
        ], $tableOptions);
        $this->addForeignKey('fk-order_items-order_id', '{{%order_items}}', 'order_id', '{{%orders}}', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('fk-order_items-item_id', '{{%order_items}}', 'item_id', '{{%service_items}}', 'id', 'RESTRICT', 'RESTRICT');
    }

    public function down()
    {
        $this->dropForeignKey('fk-order_items-item_id', '{{%order_items}}');
        $this->dropForeignKey('fk-order_items-order_id', '{{%order_items}}');
        $this->dropTable('{{%order_items}}');

        $this->dropForeignKey('fk-orders-user_id', '{{%orders}}');
        $this->dropForeignKey('fk-orders-company_id', '{{%orders}}');
        $this->dropIndex('idx-orders-company_id', '{{%orders}}');
        $this->dropTable('{{%orders}}');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
