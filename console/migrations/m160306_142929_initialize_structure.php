<?php

use yii\db\Migration;

class m160306_142929_initialize_structure extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%company}}', [
            'id' => $this->primaryKey(),
            'company_name' => $this->string()->notNull(),
            'company_address' => $this->text()->notNull(),
            'company_latitude' => $this->string(),
            'company_longitude' => $this->string(),

            'status' => $this->smallInteger()->notNull()->defaultValue(10),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ], $tableOptions);

        $this->createIndex('idx-company_id_handphone', '{{%user}}', 'company_id, handphone', TRUE);
        $this->addForeignKey('fk-user-company_id', '{{%user}}', 'company_id', '{{%company}}', 'id', 'CASCADE', 'CASCADE');

        $this->createTable('{{%services}}', [
            'id' => $this->primaryKey(),
            'company_id' => $this->integer()->notNull(),
            'service_name' => $this->string(32)->notNull(),
            'service_unit' => $this->string(10)->notNull(),
            'price_per_unit' => $this->decimal(20, 2)->notNull(),
        ], $tableOptions);
        $this->addForeignKey('fk-services-company_id', '{{%services}}', 'company_id', '{{%company}}', 'id', 'CASCADE', 'CASCADE');

        $this->createTable('{{%service_items}}', [
            'id' => $this->primaryKey(),
            'company_id' => $this->integer()->notNull(),
            'item_name' => $this->string(32)->notNull(),
            'service_ids' => $this->string()->notNull(), // available in wich services
        ], $tableOptions);
        $this->addForeignKey('fk-service_items-company_id', '{{%service_items}}', 'company_id', '{{%company}}', 'id', 'CASCADE', 'CASCADE');
    }

    public function down()
    {
        $this->dropForeignKey('fk-service_items-company_id', '{{%service_items}}');
        $this->dropTable('{{%service_items}}');

        $this->dropForeignKey('fk-services-company_id', '{{%servces}}');
        $this->dropTable('{{%services}}');

        $this->dropForeignKey('fk-user-company_id', '{{%user}}');
        $this->dropIndex('idx-company_id_handphone', '{{%user}}');
        $this->dropTable('{{%company}}');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
