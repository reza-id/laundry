<?php

use yii\db\Migration;

class m160307_151130_order_to_service_relation extends Migration
{
    public function up()
    {
        $this->addColumn('{{%orders}}', 'service_id', $this->integer());
        $this->addForeignKey('fk-orders-service_id', '{{%orders}}', 'service_id', '{{%services}}', 'id', 'SET NULL', 'CASCADE');
    }

    public function down()
    {
        $this->dropForeignKey('fk-orders-service_id', '{{%orders}}');
        $this->dropColumn('{{%orders}}', 'service_id');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
