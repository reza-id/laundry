<?php

namespace frontend\controllers;

use Yii;
use common\models\Order;
use common\models\OrderItem;
use common\models\OrderSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use common\models\BaseModel;
use yii\filters\VerbFilter;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

/**
 * OrdersController implements the CRUD actions for Order model.
 */
class OrdersController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Order models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new OrderSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Order model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $modelOrder = $this->findModel($id);
        $modelsDetail = $modelOrder->orderItems;

        return $this->render('view', [
            'modelOrder' => $modelOrder,
            'modelsDetail' => $modelsDetail,
        ]);
    }

    /**
     * Creates a new Order model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        
        $modelOrder = new Order();
        $modelsDetail = [new OrderItem];

        if ($modelOrder->load(Yii::$app->request->post())) {                

            $modelsDetail = BaseModel::createMultiple(OrderItem::classname());
            BaseModel::loadMultiple($modelsDetail, Yii::$app->request->post());

            // ajax validation
            // if (Yii::$app->request->isAjax) {
            //     Yii::$app->response->format = Response::FORMAT_JSON;
            //     return ArrayHelper::merge(
            //         ActiveForm::validateMultiple($modelsDetail),
            //         ActiveForm::validate($modelOrder)
            //     );
            // }

            // validate all models
            $valid = $modelOrder->validate();
            $valid = BaseModel::validateMultiple($modelsDetail, ['item_id', 'amount']) && $valid;

            if ($valid) {
                $transaction = \Yii::$app->db->beginTransaction();
                try {
                    if ($flag = $modelOrder->save(false)) {
                        foreach ($modelsDetail as $mDetail) {
                            $mDetail->order_id = $modelOrder->id;
                            if (! ($flag = $mDetail->save(false))) {
                                $transaction->rollBack();
                                break;
                            }
                        }
                    }
                    if ($flag) {
                        $transaction->commit();
                        return $this->redirect(['view', 'id' => $modelOrder->id]);
                    }
                } catch (Exception $e) {
                    $transaction->rollBack();
                }
            }
        }

        return $this->render('create', [
            'modelOrder' => $modelOrder,
            'modelsDetail' => (empty($modelsDetail)) ? [new OrderItem] : $modelsDetail
        ]);
    }

    /**
     * Updates an existing Order model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $modelOrder = $this->findModel($id);
        $modelsDetail = $modelOrder->orderItems;

        if ($modelOrder->load(Yii::$app->request->post())) {

            $oldIDs = ArrayHelper::map($modelsDetail, 'id', 'id');
            $modelsDetail = BaseModel::createMultiple(OrderItem::classname(), $modelsDetail);
            BaseModel::loadMultiple($modelsDetail, Yii::$app->request->post());
            $deletedIDs = array_diff($oldIDs, array_filter(ArrayHelper::map($modelsDetail, 'id', 'id')));

            // ajax validation
            // if (Yii::$app->request->isAjax) {
            //     Yii::$app->response->format = Response::FORMAT_JSON;
            //     return ArrayHelper::merge(
            //         ActiveForm::validateMultiple($modelsDetail),
            //         ActiveForm::validate($modelOrder)
            //     );
            // }

            // validate all models
            $valid = $modelOrder->validate();
            $valid = BaseModel::validateMultiple($modelsDetail, ['item_id', 'amount']) && $valid;

            if ($valid) {
                $transaction = \Yii::$app->db->beginTransaction();
                try {
                    if ($flag = $modelOrder->save(false)) {
                        if (!empty($deletedIDs)) {
                            OrderItem::deleteAll(['id' => $deletedIDs]);
                        }

                        foreach ($modelsDetail as $mDetail) {
                            $mDetail->order_id = $modelOrder->id;
                            if (! ($flag = $mDetail->save(false))) {
                                $transaction->rollBack();
                                break;
                            }
                        }
                    }
                    if ($flag) {
                        $transaction->commit();
                        return $this->redirect(['view', 'id' => $modelOrder->id]);
                    }
                } catch (Exception $e) {
                    $transaction->rollBack();
                }
            }
                
        }

        return $this->render('update', [
            'modelOrder' => $modelOrder,
            'modelsDetail' => (empty($modelsDetail)) ? [new OrderItem] : $modelsDetail
        ]);
    }

    /**
     * Deletes an existing Order model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        $user_id = $model->user_id;

        if ($model->delete()) {
            Yii::$app->session->setFlash('success', 'Record  <strong>"' . $user_id . '"</strong> deleted successfully.');
        }

        return $this->redirect(['index']);
    }

    /**
     * Finds the Order model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Order the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Order::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
