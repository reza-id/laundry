<?php
namespace frontend\models;

use common\models\User;
use yii\base\Model;
use Yii;

/**
 * Signup form
 */
class SignupForm extends Model
{
    public $company_id;
    public $handphone;
    public $user_fullname;
    public $user_address;
    public $password;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['company_id' , 'required'],
            ['company_id', 'integer'],

            ['handphone', 'filter', 'filter' => 'trim'],
            ['handphone', 'required'],
            ['handphone', 'string', 'min' => 2, 'max' => 255],
            [['company_id', 'handphone'], 'unique', 'targetClass' => '\common\models\User', 'targetAttribute' => ['company_id', 'handphone'], 'message' => 'Nomer HP sudah terdaftar!'],

            ['user_fullname', 'filter', 'filter' => 'trim'],
            ['user_fullname', 'required'],
            ['user_fullname', 'string', 'max' => 255],

            ['user_address', 'filter', 'filter' => 'trim'],
            ['user_address', 'required'],
            ['user_address', 'string', 'max' => 255],

            ['password', 'required'],
            ['password', 'string', 'min' => 6],
        ];
    }

    /**
     * Signs user up.
     *
     * @return User|null the saved model or null if saving fails
     */
    public function signup()
    {
        if ($this->validate()) {
            $user = new User();
            $user->company_id = $this->company_id;
            $user->handphone = $this->handphone;
            $user->user_fullname = $this->user_fullname;
            $user->user_address = $this->user_address;
            $user->setPassword($this->password);
            $user->generateAuthKey();
            if ($user->save()) {
                return $user;
            }
        }

        return null;
    }
}
