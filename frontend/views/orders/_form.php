<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use wbraganca\dynamicform\DynamicFormWidget;

/* @var $this yii\web\View */
/* @var $modelOrder common\models\Order */
/* @var $modelsDetail common\models\OrderItems */

$js = '
jQuery(".dynamicform_wrapper").on("afterInsert", function(e, item) {
    jQuery(".dynamicform_wrapper .panel-title-orderItem").each(function(index) {
        jQuery(this).html("Item: " + (index + 1))
    });
});

jQuery(".dynamicform_wrapper").on("afterDelete", function(e) {
    jQuery(".dynamicform_wrapper .panel-title-orderItem").each(function(index) {
        jQuery(this).html("Item: " + (index + 1))
    });
});
';

$this->registerJs($js);
?>

<div class="order-form">

    <?php $form = ActiveForm::begin(['id' => 'dynamic-form']); ?>
    <div class="row">
        <div class="col-sm-6">
            <?= $form->field($modelOrder, 'company_id')->textInput() ?>
            <?= $form->field($modelOrder, 'order_date')->textInput() ?>
            <?= $form->field($modelOrder, 'order_amount')->textInput(['maxlength' => true]) ?>
            <?= $form->field($modelOrder, 'order_price')->textInput(['maxlength' => true]) ?>
            <?= $form->field($modelOrder, 'service_id')->textInput() ?>
        </div>
        <div class="col-sm-6">
            <?= $form->field($modelOrder, 'user_id')->textInput() ?>
            <?= $form->field($modelOrder, 'order_start')->textInput() ?>
            <?= $form->field($modelOrder, 'order_finish')->textInput() ?>
            <?= $form->field($modelOrder, 'order_paid')->textInput() ?>
            <?= $form->field($modelOrder, 'order_status')->textInput() ?>
        </div>
    </div>

    <div class="padding-v-md">
        <div class="line line-dashed"></div>
    </div>
    <?php DynamicFormWidget::begin([
        'widgetContainer' => 'dynamicform_wrapper', // required: only alphanumeric characters plus "_" [A-Za-z0-9_]
        'widgetBody' => '.container-items', // required: css class selector
        'widgetItem' => '.item', // required: css class
        'limit' => 4, // the maximum times, an element can be cloned (default 999)
        'min' => 0, // 0 or 1 (default 1)
        'insertButton' => '.add-item', // css class
        'deleteButton' => '.remove-item', // css class
        'model' => $modelsDetail[0],
        'formId' => 'dynamic-form',
        'formFields' => [
            'item_id',
            'amount',
        ],
    ]); ?>
    <div class="panel panel-default">
        <div class="panel-heading">
            <i class="fa fa-envelope"></i> Order Details
            <button type="button" class="pull-right add-item btn btn-success btn-xs"><i class="fa fa-plus"></i> Add Item</button>
            <div class="clearfix"></div>
        </div>
        <div class="panel-body container-items"><!-- widgetContainer -->
            <?php foreach ($modelsDetail as $index => $modelsDetail): ?>
                <div class="item panel panel-default"><!-- widgetBody -->
                    <div class="panel-heading">
                        <span class="panel-title-orderItem">Item: <?= ($index + 1) ?></span>
                        <button type="button" class="pull-right remove-item btn btn-danger btn-xs"><i class="fa fa-minus"></i></button>
                        <div class="clearfix"></div>
                    </div>
                    <div class="panel-body">
                        <?php
                            // necessary for update action.
                            if (!$modelsDetail->isNewRecord) {
                                echo Html::activeHiddenInput($modelsDetail, "[{$index}]id");
                            }
                        ?>
                        <div class="row">
                            <div class="col-sm-6">
                                <?= $form->field($modelsDetail, "[{$index}]item_id")->textInput() ?>
                            </div>
                            <div class="col-sm-6">
                                <?= $form->field($modelsDetail, "[{$index}]amount")->textInput() ?>
                            </div>
                        </div><!-- end:row -->
                    </div>
                </div>
            <?php endforeach; ?>
        </div>
    </div>
    <?php DynamicFormWidget::end(); ?>

    <div class="form-group">
        <?= Html::submitButton($modelOrder->isNewRecord ? 'Create' : 'Update', ['class' => 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>