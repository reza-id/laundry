<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $modelOrder common\models\Order */
/* @var $modelsDetail common\models\OrderItem */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Order',
]) . ' ' . $modelOrder->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Orders'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $modelOrder->id, 'url' => ['view', 'id' => $modelOrder->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="order-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'modelOrder' => $modelOrder,
        'modelsDetail' => $modelsDetail,
    ]) ?>

</div>
