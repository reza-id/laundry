<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $modelOrder common\models\Order */
/* @var $modelsDetail common\models\OrderItem */

$this->title = Yii::t('app', 'Create Order');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Orders'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="order-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'modelOrder' => $modelOrder,
        'modelsDetail' => $modelsDetail,
    ]) ?>

</div>
