<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use common\models\Company;

/* @var $this yii\web\View */
/* @var $model common\models\ServiceItem */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="service-item-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'company_id')->label('Company')->dropDownList(ArrayHelper::map(Company::find()->all(), 'id', 'company_name'), ['prompt' => '--- choose']); ?>

    <?= $form->field($model, 'item_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'service_ids')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
