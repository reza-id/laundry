<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\ServiceItem */

$this->title = Yii::t('app', 'Create Service Item');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Service Items'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="service-item-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
